import React from 'react';
import ReactDOM from 'react-dom';

const App = () => (
<header id="site-header" role="banner" itemscope="" itemtype="http://schema.org/WPHeader">
    <div id="header">
        <div class="container clearfix">
            <div class="logo-wrap">
                <h1 id="logo" class="image-logo" itemprop="headline">
                    <a href="https://demo.mythemeshop.com/coupon">
                        <img src="./static/images/logo.png" alt="Coupon" width="120" height="27"/>
                    </a>
                </h1>
                <div class="site-description" itemprop="description">
                </div>
            </div>
            <div class="header-login">
                <ul>
                <li><a href="https://demo.mythemeshop.com/coupon/wp-login.php" title="Login">Log in</a></li>
                <li><a href="https://demo.mythemeshop.com/coupon/wp-login.php?action=register">Sign up</a></li>
                </ul>
            </div>
            <div class="header-search">
                <form method="get" id="searchform" class="search-form" action="https://demo.mythemeshop.com/coupon" _lpchecked="1">
                <fieldset>
                    <input type="text" name="s" id="s" value="" placeholder="Search for eBay, Amazon, Pizza etc."/>
                    <input type="hidden" class="mts_post_type" name="post_type" value="coupons"/>
                    <button id="search-image" class="sbutton" type="submit" value="">
                    <i class="fa fa-search"></i>
                    </button>
                </fieldset>
                </form>
            </div>
        </div>
    </div>
    <div id="primary-navigation" class="clearfix" role="navigation" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
        <div class="container clearfix">
            <a href="https://demo.mythemeshop.com/coupon/#" id="pull" class="toggle-mobile-menu">Menu</a>
            <nav class="navigation clearfix mobile-menu-wrapper">
                <ul id="menu-menu" class="menu clearfix toggle-menu">
                <li id="menu-item-470" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-470">
                    <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/">Home</a>
                    
                </li>
                <li id="menu-item-594" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-594">
                    <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/coupons/">Coupons</a>
                    
                </li>
                <li id="menu-item-557" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-557">
                    <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/blog/">Blog</a>
                    
                </li>
                <li id="menu-item-539" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-539 toggle-menu-item-parent">
                    <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/">Options Panel</a>
                    
                    <ul class="sub-menu toggle-submenu" style={{ display: 'none' }}>
                        <li id="menu-item-538" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-538 toggle-menu-item-parent">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/general-settings/">General Settings</a>
                            
                            <ul class="sub-menu toggle-submenu" style={{ display: 'none' }}>
                            <li id="menu-item-548" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-548">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/typography-settings/">Typography Settings</a>
                                <style type="text/css"></style>
                            </li>
                            <li id="menu-item-549" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-549">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/import-export-settings/">Import/Export Settings</a>
                                <style type="text/css"></style>
                            </li>
                            </ul>
                            <span class="toggle-caret"><i class="fa fa-plus"></i></span>
                        </li>
                        <li id="menu-item-550" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-550">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/performance-settings/">Performance Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-545" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-545">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/styling-settings/">Styling Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-528" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-528">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/header-settings/">Header Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-546" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-546">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/footer-settings/">Footer Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-537" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-537 toggle-menu-item-parent">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/homepage-settings/">HomePage Settings</a>
                            <style type="text/css"></style>
                            <ul class="sub-menu toggle-submenu" style={{ display: 'none' }}>
                            <li id="menu-item-675" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-675">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/homepage-settings/carousel-settings/">Carousel Settings</a>
                                <style type="text/css"></style>
                            </li>
                            <li id="menu-item-674" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-674">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/homepage-settings/slider-settings/">Slider Settings</a>
                                <style type="text/css"></style>
                            </li>
                            <li id="menu-item-673" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-673">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/homepage-settings/category-tabs-settings/">Category Tabs Settings</a>
                                <style type="text/css"></style>
                            </li>
                            <li id="menu-item-672" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-672">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/homepage-settings/social-icons-settings/">Social Icons Settings</a>
                                <style type="text/css"></style>
                            </li>
                            <li id="menu-item-671" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-671">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/homepage-settings/popular-store-settings/">Popular Store Settings</a>
                                <style type="text/css"></style>
                            </li>
                            <li id="menu-item-670" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-670">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/homepage-settings/subscribe-settings/">Subscribe Settings</a>
                                <style type="text/css"></style>
                            </li>
                            <li id="menu-item-669" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-669">
                                <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/homepage-settings/sign-up-settings/">Sign Up Settings</a>
                                <style type="text/css"></style>
                            </li>
                            </ul>
                            <span class="toggle-caret"><i class="fa fa-plus"></i></span>
                        </li>
                        <li id="menu-item-668" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-668">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/coupon-archive-settings/">Coupon Archive Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-667" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-667">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/coupon-single-page/">Coupon Single Page</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-529" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-529">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/single-post-settings/">Single Post Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-530" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-530">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/social-settings/">Social Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-547" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-547">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/sidebar-settings/">Sidebar Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-541">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/ad-settings/">Ad Settings</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-666" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-666">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/options-panel/translations-settings/">Translations Settings</a>
                            <style type="text/css"></style>
                        </li>
                    </ul>
                    <span class="toggle-caret"><i class="fa fa-plus"></i></span>
                </li>
                <li id="menu-item-542" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-542 toggle-menu-item-parent">
                    <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/pages/">Pages</a>
                    <style type="text/css"></style>
                    <ul class="sub-menu toggle-submenu" style={{ display: 'none' }}>
                        <li id="menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-543">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/page-right-sidebar/">Page [Right Sidebar]</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-544">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/icons/">Icons [No Sidebar]</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-540" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-540">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/parallax-page/">Parallax Page</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-552" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-552">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/zoomout-page/">ZoomOut Page</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-551" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-551">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/contact-page/">Contact Page</a>
                            <style type="text/css"></style>
                        </li>
                        <li id="menu-item-553" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-553">
                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/shortcodes/">Shortcodes</a>
                            <style type="text/css"></style>
                        </li>
                    </ul>
                    <span class="toggle-caret"><i class="fa fa-plus"></i></span>
                </li>
                <li id="menu-item-554" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-554">
                    <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/shop/">Shop</a>
                    <style type="text/css"></style>
                </li>
                </ul>
            </nav>
        </div>
    </div>
    </header>
)
ReactDOM.render(<App/>, document.getElementById('root'));