import * as React from 'react'


export class Footer extends React.Component {
    constructor(props: any) {
        super(props)
    }
    public render(){
        return (
            <footer id="site-footer" role="contentinfo">
                <div className="container">
                    <div className="footer-widgets first-footer-widgets widgets-num-4">
                        <div className="f-widget f-widget-1">
                            <div id="mts_site_stats_widget-2" className="widget widget_mts_site_stats_widget horizontal-small">
                                <ul className="popular-posts stats-widget">
                                    <li className="post-box">
                                        <div className="stats-icon">
                                            <i className="fa fa-tag"></i>                    
                                        </div>
                                        <div className="stats-right">
                                            <div className="stats-number">
                                                21,390                        
                                            </div>
                                            <div className="stats-text">
                                                Coupons redeemed                        
                                            </div>
                                        </div>
                                    </li>
                                    <li className="post-box">
                                        <div className="stats-icon">
                                            <i className="fa fa-shopping-bag"></i>                    
                                        </div>
                                        <div className="stats-right">
                                            <div className="stats-number">
                                                1,401                        
                                            </div>
                                            <div className="stats-text">
                                                Coupons &amp; Deals for you                        
                                            </div>
                                        </div>
                                    </li>
                                    <li className="post-box">
                                        <div className="stats-icon">
                                            <i className="fa fa-user"></i>                    
                                        </div>
                                        <div className="stats-right">
                                            <div className="stats-number">
                                                50,000+                        
                                            </div>
                                            <div className="stats-text">
                                                Happy Users                        
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="f-widget f-widget-2">
                            <div id="nav_menu-2" className="widget widget_nav_menu toggle-menu">
                                <h3 className="widget-title">More Links</h3>
                                <div className="menu-footer-container">
                                    <ul id="menu-footer" className="menu">
                                        <li id="menu-item-678" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-678">
                                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/blog/">Blog</a>
                                            <style type="text/css"></style>
                                        </li>
                                        <li id="menu-item-679" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-679">
                                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/cart/">Cart</a>
                                            <style type="text/css"></style>
                                        </li>
                                        <li id="menu-item-681" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-681">
                                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/shop/">Shop</a>
                                            <style type="text/css"></style>
                                        </li>
                                        <li id="menu-item-682" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-682">
                                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/shortcodes/">Shortcodes</a>
                                            <style type="text/css"></style>
                                        </li>
                                        <li id="menu-item-680" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-680">
                                            <a title="" target="" rel="" href="https://demo.mythemeshop.com/coupon/contact-page/">Contact Page</a>
                                            <style type="text/css"></style>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="f-widget f-widget-3">
                            <div id="text-2" className="widget widget_text">
                                <h3 className="widget-title">About Coupon</h3>
                                <div className="textwidget">
                                    Donec vel ante tincidunt, volutpat felis sit amet, porttitor sem. Sed blandit vulputate augue. Morbi id nisi lorem.
                                    <br/><br/>
                                    In non ullamcorper neque. Vestibulum lacinia diam ac consequat tincidunt porttitor sem
                                </div>
                            </div>
                        </div>
                        <div className="f-widget last f-widget-4">
                            <div id="text-3" className="widget widget_text">
                                <div className="textwidget"><a href="https://demo.mythemeshop.com/coupon/"><img src="./static/images/logo.png" alt="Coupon" /></a></div>
                            </div>
                            <div id="mts_coupon_app_widget-2" className="widget widget_mts_coupon_app_widget horizontal-small">
                                <ul className="popular-posts coupon-app">
                                    <li className="post-box">
                                        <a className="app-container clearfix" href="https://demo.mythemeshop.com/coupon/#">
                                            <div className="app-icon">
                                                <i className="fa fa-android"></i>                        
                                            </div>
                                            <div className="app-right">
                                                <div className="app-small">
                                                    Available for                            
                                                </div>
                                                <div className="app-large">
                                                    Android                            
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="post-box">
                                        <a className="app-container clearfix" href="https://demo.mythemeshop.com/coupon/#">
                                            <div className="app-icon">
                                                <i className="fa fa-apple"></i>                        
                                            </div>
                                            <div className="app-right">
                                                <div className="app-small">
                                                    Available for                            
                                                </div>
                                                <div className="app-large">
                                                    App Store                            
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="footer-info-section clearfix">
                        <div className="info-text">
                            <div className="info-title">Vestibulum elementum convallis porttitor</div>
                            <div className="info-content">Ut placerat consequat diam, sed placerat justo sagittis nec. Suspendisse tempor efficitur dolor at tempor. Donec commodo, orci, dui nibh imperdiet neque, sed volutpat orci sem nec est.</div>
                        </div>
                        <div className="footer-social-icons">
                            <div className="footer-title">Follow Us on</div>
                            <div className="footer-social">
                                <a href="https://facebook.com/mythemeshop" className="footer-facebook"><span className="fa fa-facebook"></span></a>
                                <a href="https://twitter.com/mythemeshopteam" className="footer-twitter"><span className="fa fa-twitter"></span></a>
                                <a href="https://google.com/+MyThemeShop" className="footer-google-plus"><span className="fa fa-google-plus"></span></a>
                                <a href="https://demo.mythemeshop.com/coupon/#" className="footer-instagram"><span className="fa fa-instagram"></span></a>
                                <a href="https://demo.mythemeshop.com/coupon/#" className="footer-pinterest-p"><span className="fa fa-pinterest-p"></span></a>
                                <a href="http://feeds.feedburner.com/mythemeshop" className="footer-rss"><span className="fa fa-rss"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="copyrights">
                    <div className="container">
                        <div className="row" id="copyright-note">
                            <span><a href="https://demo.mythemeshop.com/coupon/" title=" Coupon WordPress Theme">Coupon</a> Copyright © 2018.</span>
                            <div className="to-top">Theme by <a href="https://mythemeshop.com/" rel="nofollow">MyThemeShop</a></div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}