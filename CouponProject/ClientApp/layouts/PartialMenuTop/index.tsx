import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { Menu } from './models';

var dataMenu = require('./dataMenu.json');

interface PartialMenuTopProps {
    // 'id': 'Coupons',
    // 'name': 'Coupons',
    // 'link': '/threads/list',
    // 'icon': '#next-icon-list',
    // 'hasarrow': false,
    // 'childs': [{}],
    // 'callback': null
}

interface PartialMenuTopStates {
    
}

export class MenuTop extends React.Component {
    constructor(props: any) {
        super(props)
    }

    private handleActive () {

    }

    private initMenuData() {
        let data : Menu = dataMenu.map((item: any, index: any) => {
            let classname = item.class;
            if (item.active) {
                classname += ' current-menu-item';
            }
            return {
                id: item.id,
                name: item.name,
                title: item.title,
                rel: item.rel,
                target: item.target,
                link: item.link,
                icon: item.icon,
                class: classname,
                active: item.active,
                hasarrow: item.hasarrow,
                childs: item.childs,
                callback: item.callback
            }
        });
        return data;
    }

    public renderMenu(list:any) {
        return list.map((item: any, index: any) => {
            return (
                <li key={item.id} className={item.class}>
                    {/* <a href={item.link}>{item.name}</a> */}
                    <NavLink to={'/Test1'} className='ps-default'>
                        {item.name}
                    </NavLink>
                    {(item.childs && item.childs.length > 0) &&
                        <ul className="sub-menu toggle-submenu" style={{ display: 'none' }}>
                            {this.renderMenu(item.childs)}
                        </ul>
                    }
                </li>
            );
        });
    }

    public render() {
        let data = this.initMenuData();
        return (
            <div id="primary-navigation" className="clearfix" role="navigation">
                <div className="container clearfix">
                    <a href="#" id="pull" className="toggle-mobile-menu">Menu</a>
                    <nav className="navigation clearfix mobile-menu-wrapper">
                        <ul id="menu-menu" className="menu clearfix toggle-menu">
                            {this.renderMenu(data)}
                        </ul>
                    </nav>
                </div>
            </div>
        );
    }
}