export interface Menu {
    id: string,
    name: string,
    title: string,
    rel: string,
    target: string,
    link: string,
    icon: string,
    class: string,
    active: string,
    hasarrow: string,
    childs: Menu,
    callback: string
}