import * as React from 'react';
import { MenuTop } from '../PartialMenuTop';
import './index.less';

var logo = require('../../assets/images/logo.png');

interface HeaderProps {
    OnMenuClick: Function
}

export class Header extends React.Component {
    constructor(props: any) {
        super(props);
    }

    public render() {
        return (
            <header id="site-header" role="banner">
                <div id="header">
                    <div className="container clearfix">
                        <div className="logo-wrap">
                            <h1 id="logo" className="image-logo">
                                <a href="https://demo.mythemeshop.com/coupon">
                                    <img src={logo} alt="Coupon" width="120" height="27"/>
                                </a>
                            </h1>
                            <div className="site-description">
                            </div>
                        </div>
                        <div className="header-login">
                            <ul>
                            <li><a href="https://demo.mythemeshop.com/coupon/wp-login.php" title="Login">Log in</a></li>
                            <li><a href="https://demo.mythemeshop.com/coupon/wp-login.php?action=register">Sign up</a></li>
                            </ul>
                        </div>
                        <div className="header-search">
                            <form method="get" id="searchform" className="search-form" action="https://demo.mythemeshop.com/coupon">
                            <fieldset>
                                <input type="text" name="s" id="s" value="" placeholder="Search for eBay, Amazon, Pizza etc."/>
                                <input type="hidden" className="mts_post_type" name="post_type" value="coupons"/>
                                <button id="search-image" className="sbutton" type="submit" value="">
                                <i className="fa fa-search"></i>
                                </button>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                    <MenuTop/>
                </div>
            </header>
        )
    }
}

