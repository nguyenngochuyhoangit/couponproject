﻿import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router, Route, Switch, Redirect, BrowserRouter } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { createStore } from 'redux'

import { Layout } from './layouts/Layout';
import { Test1 } from './screens/Test1/test1';
import { Test2 } from './screens/Test2/test2';
import { Home } from './screens/Home';

// import './assets/css/font-awesome.min.css';
import './assets/css/wpmm.css';
import './assets/css/style.min.css';
import './assets/css/magnific-popup.css';
// import './assets/css/wp-review.css';
import './assets/css/owl.carousel.css';
import './assets/css/responsive.css';
import './assets/js/jquery.js';
import './assets/js/jquery-migrate.min.js';
import './assets/css/style.css';
import './assets/css/css.css';

const store = createStore(rootReducer);

export const history = createBrowserHistory();

export class App extends React.Component {
    public render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Router history={history}>
                        <Layout>
                            <Switch>
                                <Route path = "/Home" component = {Home} />
                                <Route path = "/Test1" component = {Test1} />
                                <Route path = "/Test2" component = {Test2} />
                            </Switch>
                        </Layout>
                    </Router>
                </BrowserRouter>
            </Provider>
        )
    }
}
ReactDOM.render(<App />, document.getElementById('react-app'));