import * as React from 'react';

var ebayLogo = require('../../assets/images/ebay-logo.png');

// const owlStage = {
//     transform: translate3d(-1200px, 0px, 0px),
//     transition: all 0s ease 0s; width: 3600px
// };

const owlItem = {
    width: '180px',
     marginRight: '20px'
}

export class Home extends React.Component {
    public render() {
        return (
            <div>
                <div id="homepage">
                    <div className="container coupon-carousel-container">
                    <h3 className="featured-category-title">Today's Offers and Coupons</h3>
                    <div id="slider" className="coupon-carousel owl-carousel owl-theme owl-loaded">
                        <div className="owl-stage-outer">
                            <div className="owl-stage" style={{transform: 'translate3d(-1200px, 0px, 0px)', transition: 'all 0s ease 0s', width: '3600px'}}>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-1">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/clothing/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" className="slide-first-img" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Upto 40% Cashback  on Recharge</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-2">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/electronics/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">$2 Cashback on Mobile Recharges</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-3">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/food/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Signup &amp; Get $10  for Free</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-4">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/travel/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Flat $6 Off on Ciaz City Amaze Cars</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-5">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/clothing/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">70% OFF On Men's Fashion</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-6">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/electronics/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Flat 25% OFF On  $300 &amp; Above</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item active" style={owlItem}>
                                <div className="owl-item-carousel count-1">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/clothing/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" className="slide-first-img" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Upto 40% Cashback  on Recharge</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item active" style={owlItem}>
                                <div className="owl-item-carousel count-2">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/electronics/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">$2 Cashback on Mobile Recharges</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item active" style={owlItem}>
                                <div className="owl-item-carousel count-3">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/food/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Signup &amp; Get $10  for Free</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item active" style={owlItem}>
                                <div className="owl-item-carousel count-4">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/travel/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Flat $6 Off on Ciaz City Amaze Cars</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item active" style={owlItem}>
                                <div className="owl-item-carousel count-5">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/clothing/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">70% OFF On Men's Fashion</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item active" style={owlItem}>
                                <div className="owl-item-carousel count-6">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/electronics/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Flat 25% OFF On  $300 &amp; Above</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-1">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/clothing/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" className="slide-first-img" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Upto 40% Cashback  on Recharge</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-2">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/electronics/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">$2 Cashback on Mobile Recharges</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-3">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/food/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Signup &amp; Get $10  for Free</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-4">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/travel/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Flat $6 Off on Ciaz City Amaze Cars</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-5">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/clothing/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">70% OFF On Men's Fashion</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={owlItem}>
                                <div className="owl-item-carousel count-6">
                                    <a href="https://demo.mythemeshop.com/coupon/coupons-category/electronics/">
                                        <div className="coupon-carousel-wrapper clearfix">
                                        <img src={ebayLogo} alt="" width="auto" height="26" />
                                        <div className="slide-caption">
                                            <div className="slide-caption-inner">
                                                <h2 className="slide-title">Flat 25% OFF On  $300 &amp; Above</h2>
                                            </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div className="owl-controls">
                            <div className="owl-nav">
                            <div className="owl-prev"><i className="fa fa-angle-left"><i></i></i></div>
                            <div className="owl-next"><i className="fa fa-angle-right"><i></i></i></div>
                            </div>
                            <div className="owl-dots" style={{display: 'none'}}></div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}