﻿const path = require('path');
const bundleOutputDir = './wwwroot/dist';

// const HWP = require('html-webpack-plugin');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanCSSPlugin = require('less-plugin-clean-css');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CheckerPlugin = require('awesome-typescript-loader').CheckerPlugin;

var config = function(env, argv){
    console.log('wellcom mode ' + argv.mode);
    const isDevBuild = argv.mode !== 'production' ? true : false;

    return {
        entry: {
            main: [
                path.join(__dirname, '/ClientApp/index.tsx'),
                // path.join(__dirname, '/ClientApp/style.less'),
                // path.join(__dirname, '/ClientApp/layouts/Header/index.less')
            ]
        },
        watch:true,
        resolve: {
            // Look for modules in .ts(x) files first, then .js
            extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css", ".less"]
        },
        optimization: {
            minimizer: isDevBuild ? [] : [new UglifyJsPlugin({ cache: true, parallel: true, sourceMap: true }), new OptimizeCSSAssetsPlugin({})]
        },
        output: {
            filename:   "[name].js",
            path: path.resolve(__dirname, bundleOutputDir)
        },
        module: {
            rules: [
                { test: /\.tsx$/, exclude: /node_modules/, loader: 'awesome-typescript-loader' },
                // { test: /\.jsx$/, exclude: /node_modules/, loader: 'babel-loader' },
                { test: /.(png|jpg|jpeg|gif|svg|woff|woff2|eot|ttf)(\?v=\d+\.\d+\.\d+)?$/, use: [{
                                                                                                    loader: 'url-loader',
                                                                                                    options: { 
                                                                                                        limit: 25000, // Convert images < 8kb to base64 strings
                                                                                                        // mimetype: 'assets/images'
                                                                                                        // name: 'assets/images/[hash]-[name].[ext]'
                                                                                                    } 
                                                                                                }] 
                }, //'url-loader?limit=25000'
                { test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.wav$|\.mp3$/, use: [{
                                                                                        loader: 'image-webpack-loader',
                                                                                        options: { 
                                                                                            name: '[path][name].[ext]',
                                                                                        } 
                                                                                        
                                                                                    }] 
                }, //'file-loader?name=[path][name].[ext]' },  // <-- retain original file name 
                { test: /\.(less|css)$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'less-loader', options: isDevBuild ? {
                            } : {
                                plugins: [
                                  new CleanCSSPlugin({ advanced: true })
                                ]
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new CheckerPlugin(),
            new MiniCssExtractPlugin({
               filename: "css/main.css",
               chunkFilename: "css/main.css"
            })
            // //new HWP({ template: path.join(__dirname, '/src/index.html') })
            // new ExtractTextPlugin('[name].css')
        ]
    }
};

module.exports = config;